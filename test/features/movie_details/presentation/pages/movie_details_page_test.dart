import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:network_image_mock/network_image_mock.dart';
import 'package:yologroup_assignment/data/movie_details/models/movie_details_model.dart';
import 'package:yologroup_assignment/dependency_injection.dart';
import 'package:yologroup_assignment/presentation/movie_details/cubit/movie_details_cubit.dart';
import 'package:yologroup_assignment/presentation/movie_details/cubit/movie_details_state.dart';
import 'package:yologroup_assignment/presentation/movie_details/pages/movie_details_page.dart';
import 'package:yologroup_assignment/presentation/movie_details/widgets/movie_details_widget.dart';
import 'package:yologroup_assignment/presentation/movies_list/widgets/loading_widget.dart';
import 'package:yologroup_assignment/presentation/movies_list/widgets/message_display_widget.dart';
import '../../data/repository/movie_details_repository_impl_test.mocks.dart';

void main() {
  late MovieDetailsCubit mockCubit;

  setUp(() {
    mockCubit = MockMovieDetailsCubit();
    sl.registerFactory<MovieDetailsCubit>(() => mockCubit);
  });

  tearDown(() => sl.reset());

  group('MovieDetailsPage', () {
    const movieId = 1;

    testWidgets('renders LoadingWidget when state is MovieDetailsLoadingState',
        (WidgetTester tester) async {
      when(mockCubit.state).thenReturn(MovieDetailsLoadingState());

      await tester.pumpWidget(
        MaterialApp(
          home: BlocProvider.value(
            value: mockCubit,
            child: const MovieDetailsPage(movieId: movieId),
          ),
        ),
      );

      expect(find.byType(LoadingWidget), findsOneWidget);
    });

    testWidgets(
        'renders MovieDetailsWidget when state is MovieDetailsLoadedState',
        (WidgetTester tester) async {
      when(mockCubit.state).thenReturn(
        MovieDetailsLoadedState(
            movieDetails: const MovieDetailsModel(
                id: 12, video: true, release_date: '12 feb', adult: false)),
      );

      await mockNetworkImagesFor(() => tester.pumpWidget(
            MaterialApp(
              home: Scaffold(
                body: BlocProvider.value(
                  value: mockCubit,
                  child: const MovieDetailsPage(movieId: movieId),
                ),
              ),
            ),
          ));

      expect(find.byType(MovieDetailsWidget), findsOneWidget);
    });

    testWidgets('renders MessageDisplay when state is MovieDetailsInitialState',
        (WidgetTester tester) async {
      when(mockCubit.state).thenReturn(MovieDetailsInitialState());

      await tester.pumpWidget(
        MaterialApp(
          home: BlocProvider.value(
            value: mockCubit,
            child: const MovieDetailsPage(movieId: movieId),
          ),
        ),
      );

      expect(find.byType(MessageDisplay), findsOneWidget);
    });

    testWidgets('renders MessageDisplay when state is MovieDetailsErrorState',
        (WidgetTester tester) async {
      when(mockCubit.state).thenReturn(
        MovieDetailsErrorState(message: 'Error'),
      );

      await tester.pumpWidget(
        MaterialApp(
          home: BlocProvider.value(
            value: mockCubit,
            child: const MovieDetailsPage(movieId: movieId),
          ),
        ),
      );

      expect(find.byType(MessageDisplay), findsOneWidget);
    });
  });
}
