import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:network_image_mock/network_image_mock.dart';
import 'package:yologroup_assignment/data/movie_details/models/movie_details_model.dart';
import 'package:yologroup_assignment/presentation/movie_details/widgets/movie_details_image_widget.dart';
import 'package:yologroup_assignment/presentation/movie_details/widgets/movie_info_row_widget.dart';
import 'package:yologroup_assignment/presentation/movie_details/widgets/movie_details_widget.dart';

void main() {
  group('MovieDetailsWidget', () {
    const movieDetails = MovieDetailsModel(
        title: 'Spiderman',
        overview: 'This is a Spiderman',
        adult: false,
        video: false,
        release_date: '12 feb');

    testWidgets('renders correct title and overview',
        (WidgetTester tester) async {
      await mockNetworkImagesFor(() => tester.pumpWidget(
            const MaterialApp(
              home: Scaffold(
                body: MovieDetailsWidget(movieDetails: movieDetails),
              ),
            ),
          ));

      expect(find.text(movieDetails.title ?? ''), findsOneWidget);
      expect(find.text(movieDetails.overview ?? ''), findsOneWidget);
    });

    testWidgets('renders MovieDetailsImageWidget', (WidgetTester tester) async {
      await mockNetworkImagesFor(() => tester.pumpWidget(
            const MaterialApp(
              home: Scaffold(
                body: MovieDetailsWidget(movieDetails: movieDetails),
              ),
            ),
          ));
      expect(find.byType(MovieDetailsImageWidget), findsOneWidget);
    });

    testWidgets('renders MovieInfoRowWidget', (WidgetTester tester) async {
      await mockNetworkImagesFor(() => tester.pumpWidget(
            const MaterialApp(
              home: Scaffold(
                body: MovieDetailsWidget(movieDetails: movieDetails),
              ),
            ),
          ));

      expect(find.byType(MovieInfoRowWidget), findsOneWidget);
    });
  });
}
