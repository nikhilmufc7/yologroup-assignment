import 'package:test/test.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:mockito/mockito.dart';
import 'package:yologroup_assignment/data/movie_details/models/movie_details_model.dart';
import 'package:yologroup_assignment/presentation/movie_details/cubit/movie_details_cubit.dart';
import 'package:yologroup_assignment/presentation/movie_details/cubit/movie_details_state.dart';

import '../../data/repository/movie_details_repository_impl_test.mocks.dart';

void main() {
  late MovieDetailsCubit cubit;
  late MockGetMovieDetailsUsecase mockUsecase;

  setUp(() {
    mockUsecase = MockGetMovieDetailsUsecase();
    cubit = MovieDetailsCubit(mockUsecase);
  });

  group('MovieDetailsCubit', () {
    const movieId = 1;

    test('initial state should be MovieDetailsInitialState', () {
      expect(cubit.state, equals(MovieDetailsInitialState()));
    });

    blocTest<MovieDetailsCubit, MovieDetailsState>(
      'should emit Loading and Loaded states when getMovieDetails succeeds',
      build: () {
        when(mockUsecase.call(any)).thenAnswer(
          (_) async => const Right(MovieDetailsModel(id: 123)),
        );
        return cubit;
      },
      act: (cubit) => cubit.getMovieDetails(movieId),
      expect: () => [
        MovieDetailsLoadingState(),
        MovieDetailsLoadedState(movieDetails: const MovieDetailsModel(id: 123)),
      ],
    );

    blocTest<MovieDetailsCubit, MovieDetailsState>(
      'should emit Loading and Error states when getMovieDetails fails',
      build: () {
        when(mockUsecase.call(any)).thenAnswer(
          (_) async => Left(fail('fail')),
        );
        return cubit;
      },
      act: (cubit) => cubit.getMovieDetails(movieId),
      expect: () => [
        MovieDetailsLoadingState(),
        MovieDetailsErrorState(message: 'fail'),
      ],
    );

    blocTest<MovieDetailsCubit, MovieDetailsState>(
      'should emit Loading and Error states when getMovieDetails throws an exception',
      build: () {
        when(mockUsecase.call(any)).thenThrow(Exception('Error'));
        return cubit;
      },
      act: (cubit) => cubit.getMovieDetails(movieId),
      expect: () => [
        MovieDetailsLoadingState(),
        MovieDetailsErrorState(message: 'Exception: Error'),
      ],
    );
  });
}
