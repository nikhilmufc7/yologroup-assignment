import 'package:test/test.dart';
import 'package:dartz/dartz.dart';
import 'package:mockito/mockito.dart';
import 'package:yologroup_assignment/common/constants/constants.dart';
import 'package:yologroup_assignment/common/error/failures.dart';
import 'package:yologroup_assignment/data/movie_details/models/movie_details_model.dart';
import 'package:yologroup_assignment/domain/movie_details/usecase/movie_details_usecase.dart';

import '../data/repository/movie_details_repository_impl_test.mocks.dart';

void main() {
  late GetMovieDetailsUsecase usecase;
  late MockMovieDetailsRepository mockRepository;

  setUp(() {
    mockRepository = MockMovieDetailsRepository();
    usecase = GetMovieDetailsUsecase(mockRepository);
  });

  group('GetMovieDetailsUsecase', () {
    const movieId = 1;
    const apiKey = Constants.apiKey;

    test('should return MovieDetailsModel on successful response', () async {
      const movieDetails = MovieDetailsModel(id: movieId);
      when(mockRepository.getMovieDetailsRepositories(movieId, apiKey))
          .thenAnswer((_) async => const Right(movieDetails));

      final result = await usecase.call(const Params(movieId));

      expect(result, equals(const Right(movieDetails)));
      verify(mockRepository.getMovieDetailsRepositories(movieId, apiKey))
          .called(1);
    });

    test('should return ServerFailure on repository failure', () async {
      when(mockRepository.getMovieDetailsRepositories(movieId, apiKey))
          .thenAnswer((_) async => Left(ServerFailure()));

      final result = await usecase.call(const Params(movieId));

      expect(result, equals(Left(ServerFailure())));
      verify(mockRepository.getMovieDetailsRepositories(movieId, apiKey))
          .called(1);
    });
  });
}
