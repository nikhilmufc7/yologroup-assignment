import 'package:test/test.dart';
import 'package:yologroup_assignment/data/movie_details/models/movie_details_model.dart';

void main() {
  group('MovieDetailsModel', () {
    test('fromJson should correctly deserialize the JSON', () {
      const json = {
        'id': 1,
        'title': 'Spiderman',
        'vote_average': 7.5,
      };

      final movieDetails = MovieDetailsModel.fromJson(json);

      expect(movieDetails.id, equals(1));
      expect(movieDetails.title, equals('Spiderman'));
      expect(movieDetails.vote_average, equals(7.5));
    });

    test('toJson should correctly serialize the object to JSON', () {
      const movieDetails = MovieDetailsModel(
        id: 1,
        title: 'Spiderman',
        vote_average: 7.5,
      );

      final json = movieDetails.toJson();

      expect(json['id'], equals(1));
      expect(json['title'], equals('Spiderman'));
      expect(json['vote_average'], equals(7.5));
    });

    test('props should return a list of object properties', () {
      const movieDetails = MovieDetailsModel(id: 1);

      final props = movieDetails.props;

      expect(props, equals([1]));
    });
  });

  group('Genres', () {
    test('fromJson should correctly deserialize the JSON', () {
      const json = {
        'id': 1,
        'name': 'Action',
      };

      final genres = Genres.fromJson(json);

      expect(genres.id, equals(1));
      expect(genres.name, equals('Action'));
    });

    test('toJson should correctly serialize the object to JSON', () {
      const genres = Genres(id: 1, name: 'Action');

      final json = genres.toJson();

      expect(json['id'], equals(1));
      expect(json['name'], equals('Action'));
    });

    test('props should return a list of object properties', () {
      const genres = Genres(id: 1);

      final props = genres.props;

      expect(props, equals([1]));
    });
  });

  group('ProductionCompanies', () {
    test('fromJson should correctly deserialize the JSON', () {
      const json = {
        'id': 1,
        'name': 'Company',
      };

      final productionCompanies = ProductionCompanies.fromJson(json);

      expect(productionCompanies.id, equals(1));
      expect(productionCompanies.name, equals('Company'));
    });

    test('toJson should correctly serialize the object to JSON', () {
      const productionCompanies = ProductionCompanies(id: 1, name: 'Company');

      final json = productionCompanies.toJson();

      expect(json['id'], equals(1));
      expect(json['name'], equals('Company'));
    });

    test('props should return a list of object properties', () {
      const productionCompanies = ProductionCompanies(id: 1);

      final props = productionCompanies.props;

      expect(props, equals([1]));
    });
  });

  group('ProductionCountries', () {
    test('fromJson should correctly deserialize the JSON', () {
      const json = {
        'iso31661': 'US',
        'name': 'United States',
      };

      final productionCountries = ProductionCountries.fromJson(json);

      expect(productionCountries.iso31661, equals('US'));
      expect(productionCountries.name, equals('United States'));
    });

    test('toJson should correctly serialize the object to JSON', () {
      const productionCountries = ProductionCountries(
        iso31661: 'US',
        name: 'United States',
      );

      final json = productionCountries.toJson();

      expect(json['iso31661'], equals('US'));
      expect(json['name'], equals('United States'));
    });

    test('props should return a list of object properties', () {
      const productionCountries = ProductionCountries(iso31661: 'US');

      final props = productionCountries.props;

      expect(props, equals(['US']));
    });
  });

  group('SpokenLanguages', () {
    test('fromJson should correctly deserialize the JSON', () {
      const json = {
        'iso6391': 'en',
        'name': 'English',
      };

      final spokenLanguages = SpokenLanguages.fromJson(json);

      expect(spokenLanguages.iso6391, equals('en'));
      expect(spokenLanguages.name, equals('English'));
    });

    test('toJson should correctly serialize the object to JSON', () {
      const spokenLanguages = SpokenLanguages(iso6391: 'en', name: 'English');

      final json = spokenLanguages.toJson();

      expect(json['iso6391'], equals('en'));
      expect(json['name'], equals('English'));
    });
  });
}
