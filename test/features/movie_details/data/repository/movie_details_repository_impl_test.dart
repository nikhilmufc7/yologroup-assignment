import 'package:mockito/annotations.dart';
import 'package:test/test.dart';
import 'package:dartz/dartz.dart';
import 'package:mockito/mockito.dart';
import 'package:yologroup_assignment/common/error/exceptions.dart';
import 'package:yologroup_assignment/common/error/failures.dart';
import 'package:yologroup_assignment/data/movie_details/endpoint/movie_details_endpoint.dart';
import 'package:yologroup_assignment/data/movie_details/models/movie_details_model.dart';
import 'package:yologroup_assignment/data/movie_details/repository/movie_details_repository_impl.dart';
import 'package:yologroup_assignment/domain/movie_details/repository/movie_details_repository.dart';
import 'package:yologroup_assignment/domain/movie_details/usecase/movie_details_usecase.dart';
import 'package:yologroup_assignment/presentation/movie_details/cubit/movie_details_cubit.dart';
import 'movie_details_repository_impl_test.mocks.dart';

@GenerateNiceMocks([
  MockSpec<MovieDetailsEndpoint>(),
  MockSpec<GetMovieDetailsUsecase>(),
  MockSpec<MovieDetailsCubit>(),
  MockSpec<MovieDetailsRepository>()
])
void main() {
  late MovieDetailsRepositoryImpl repository;
  late MockMovieDetailsEndpoint mockEndpoint;

  setUp(() {
    mockEndpoint = MockMovieDetailsEndpoint();
    repository = MovieDetailsRepositoryImpl(
      movieDetailsApiEndpoints: mockEndpoint,
    );
  });

  group('getMovieDetailsRepositories', () {
    const movieId = 1;
    const apiKey = '123456';

    test('should return Right(MovieDetailsModel) on successful response',
        () async {
      const movieDetails = MovieDetailsModel(id: movieId);
      when(mockEndpoint.getMovieDetails(movieId, apiKey))
          .thenAnswer((_) async => movieDetails);

      final result =
          await repository.getMovieDetailsRepositories(movieId, apiKey);

      expect(result, equals(const Right(movieDetails)));
      verify(mockEndpoint.getMovieDetails(movieId, apiKey)).called(1);
    });

    test('should return Left(ServerFailure) on server exception', () async {
      when(mockEndpoint.getMovieDetails(movieId, apiKey))
          .thenThrow(ServerException());

      final result =
          await repository.getMovieDetailsRepositories(movieId, apiKey);

      expect(result, equals(Left(ServerFailure())));
      verify(mockEndpoint.getMovieDetails(movieId, apiKey)).called(1);
    });
  });
}
