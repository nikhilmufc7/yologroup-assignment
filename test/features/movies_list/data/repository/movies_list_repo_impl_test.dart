import 'package:flutter_test/flutter_test.dart';
import 'package:dartz/dartz.dart';
import 'package:yologroup_assignment/common/error/failures.dart';
import 'package:yologroup_assignment/data/movies_list/endpoint/movies_list_endpoint.dart';
import 'package:yologroup_assignment/data/movies_list/models/movies_data_model.dart';
import 'package:yologroup_assignment/data/movies_list/repository/movies_list_repo_impl.dart';
import 'package:yologroup_assignment/domain/movies_list/repository/movies_repository.dart';

import 'package:yologroup_assignment/common/error/exceptions.dart';

class MockMoviesListEndpoint implements MoviesListEndpoint {
  @override
  Future<MoviesModel> getMoviesList(
      String path, String apiKey, int page) async {
    return const MoviesModel(
      dates: Dates(maximum: '2023-07-07', minimum: '2023-07-01'),
      page: 1,
      results: [
        MovieResults(id: 123),
      ],
      total_pages: 10,
      total_results: 100,
    );
  }
}

void main() {
  group('MoviesListRepoImpl', () {
    late MoviesRepository moviesRepository;
    late MoviesListEndpoint moviesListEndpoint;

    setUp(() {
      moviesListEndpoint = MockMoviesListEndpoint();
      moviesRepository =
          MoviesListRepoImpl(moviesListEndpoint: moviesListEndpoint);
    });

    test('getMoviesRepositories() should return a valid MoviesModel', () async {
      const page = 1;
      const apiKey = '1234';
      const path = '/latest_movies';

      final result =
          await moviesRepository.getMoviesRepositories(page, apiKey, path);

      expect(result, isA<Right<Failure, MoviesModel>>());
      final moviesModel = result.getOrElse(() => throw ServerException());
      expect(moviesModel.page, 1);
      expect(moviesModel.results?.length, 1);
      expect(moviesModel.results?[0].id, 123);
    });

    test('getMoviesRepositories() should return a ServerFailure on exception',
        () async {
      const page = 1;
      const apiKey = '1234';
      const path = '/popular_movies';

      moviesListEndpoint = MockMoviesListEndpointThrowsException();
      moviesRepository =
          MoviesListRepoImpl(moviesListEndpoint: moviesListEndpoint);

      final result =
          await moviesRepository.getMoviesRepositories(page, apiKey, path);

      expect(result, Left(ServerFailure()));
    });
  });
}

class MockMoviesListEndpointThrowsException implements MoviesListEndpoint {
  @override
  Future<MoviesModel> getMoviesList(
      String path, String apiKey, int page) async {
    throw ServerException();
  }
}
