import 'package:flutter_test/flutter_test.dart';
import 'package:yologroup_assignment/data/movies_list/models/movies_data_model.dart';

void main() {
  group('MoviesModel', () {
    test('fromJson() should return a valid MoviesModel instance', () {
      final json = {
        'dates': {
          'maximum': '2023-07-07',
          'minimum': '2023-07-01',
        },
        'page': 1,
        'results': [
          {
            'adult': false,
            'backdrop_path': '/spiderman.jpg',
            'genre_ids': [28, 12],
            'id': 123,
            'original_language': 'en',
            'original_title': 'Spiderman',
            'overview': 'Spiderman',
            'popularity': 123.45,
            'poster_path': '/path_to_poster_image.jpg',
            'release_date': '2023-07-05',
            'title': 'Spiderman',
            'video': false,
            'vote_average': 7.8,
            'vote_count': 100,
          }
        ],
        'total_pages': 10,
        'total_results': 100,
      };

      final moviesModel = MoviesModel.fromJson(json);

      expect(moviesModel.dates?.maximum, '2023-07-07');
      expect(moviesModel.dates?.minimum, '2023-07-01');
      expect(moviesModel.page, 1);
      expect(moviesModel.results?.length, 1);
      expect(moviesModel.results?[0].id, 123);
    });

    test('props should return the correct list of properties', () {
      const moviesModel = MoviesModel(
        results: [
          MovieResults(id: 123),
        ],
      );

      final props = moviesModel.props;

      expect(props.length, 1);
      expect(props[0], moviesModel.results);
    });
  });

  group('Dates', () {
    test('fromJson() should return a valid Dates instance', () {
      final json = {
        'maximum': '2023-07-07',
        'minimum': '2023-07-01',
      };

      final dates = Dates.fromJson(json);

      expect(dates.maximum, '2023-07-07');
      expect(dates.minimum, '2023-07-01');
    });

    test('toJson() should return a valid JSON map', () {
      const dates = Dates(
        maximum: '2023-07-07',
        minimum: '2023-07-01',
      );

      final json = dates.toJson();

      expect(json['maximum'], '2023-07-07');
      expect(json['minimum'], '2023-07-01');
    });

    test('props should return the correct list of properties', () {
      const dates = Dates(
        maximum: '2023-07-07',
        minimum: '2023-07-01',
      );

      final props = dates.props;

      expect(props.length, 2);
      expect(props[0], dates.minimum);
      expect(props[1], dates.maximum);
    });
  });

  group('MovieResults', () {
    test('fromJson() should return a valid MovieResults instance', () {
      final json = {
        'adult': false,
        'backdrop_path': '/spiderman.jpg',
        'genre_ids': [28, 12],
        'id': 123,
        'original_language': 'en',
        'original_title': 'Spiderman',
        'overview': 'Spiderman movie',
        'popularity': 123.45,
        'poster_path': '/spiderman.jpg',
        'release_date': '2023-07-05',
        'title': 'Spiderman',
        'video': false,
        'vote_average': 7.8,
        'vote_count': 100,
      };

      final movieResults = MovieResults.fromJson(json);

      expect(movieResults.adult, false);
      expect(movieResults.backdrop_path, '/spiderman.jpg');
      expect(movieResults.genre_ids, [28, 12]);
      expect(movieResults.id, 123);
      expect(movieResults.original_language, 'en');
      expect(movieResults.original_title, 'Spiderman');
      expect(movieResults.overview, 'Spiderman movie');
      expect(movieResults.popularity, 123.45);
      expect(movieResults.poster_path, '/spiderman.jpg');
      expect(movieResults.release_date, '2023-07-05');
      expect(movieResults.title, 'Spiderman');
      expect(movieResults.video, false);
      expect(movieResults.vote_average, 7.8);
      expect(movieResults.vote_count, 100);
    });

    test('toJson() should return a valid JSON map', () {
      const movieResults = MovieResults(
        id: 123,
        adult: false,
        backdrop_path: '/spiderman.jpg',
        genre_ids: [28, 12],
        original_language: 'en',
        original_title: 'Spiderman',
        overview: 'Spiderman movie',
        popularity: 123.45,
        poster_path: '/spiderman.jpg',
        release_date: '2023-07-05',
        title: 'Spiderman',
        video: false,
        vote_average: 7.8,
        vote_count: 100,
      );

      final json = movieResults.toJson();

      expect(json['adult'], false);
      expect(json['backdrop_path'], '/spiderman.jpg');
      expect(json['genre_ids'], [28, 12]);
      expect(json['id'], 123);
      expect(json['original_language'], 'en');
      expect(json['original_title'], 'Spiderman');
      expect(json['overview'], 'Spiderman movie');
      expect(json['popularity'], 123.45);
      expect(json['poster_path'], '/spiderman.jpg');
      expect(json['release_date'], '2023-07-05');
      expect(json['title'], 'Spiderman');
      expect(json['video'], false);
      expect(json['vote_average'], 7.8);
      expect(json['vote_count'], 100);
    });

    test('props should return the correct list of properties', () {
      const movieResults = MovieResults(
        id: 123,
      );

      final props = movieResults.props;

      expect(props.length, 1);
      expect(props[0], movieResults.id);
    });
  });
}
