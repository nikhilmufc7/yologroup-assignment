import 'package:flutter_test/flutter_test.dart';
import 'package:dartz/dartz.dart';
import 'package:yologroup_assignment/common/error/failures.dart';
import 'package:yologroup_assignment/common/usecase/params.dart';
import 'package:yologroup_assignment/domain/movies_list/repository/movies_repository.dart';
import 'package:yologroup_assignment/data/movies_list/models/movies_data_model.dart';
import 'package:yologroup_assignment/domain/movies_list/usecase/get_all_popular_movies_usecase.dart';

class MockMoviesRepository implements MoviesRepository {
  @override
  Future<Either<Failure, MoviesModel>> getMoviesRepositories(
      int page, String apiKey, String path) async {
    return const Right(MoviesModel(
      dates: Dates(maximum: '2023-07-07', minimum: '2023-07-01'),
      page: 1,
      results: [
        MovieResults(id: 123),
      ],
      total_pages: 10,
      total_results: 100,
    ));
  }
}

void main() {
  group('GetAllLatestMoviesUsecase', () {
    late MoviesRepository moviesRepository;
    late GetAllPopularMoviesUsecase usecase;

    setUp(() {
      moviesRepository = MockMoviesRepository();
      usecase = GetAllPopularMoviesUsecase(moviesRepository);
    });

    test('call() should return a valid MoviesModel', () async {
      const page = 1;
      const path = '/popular_movies';
      const params = Params(page, path);

      final result = await usecase(params);

      expect(result, isA<Right<Failure, MoviesModel>>());
      final moviesModel = result.getOrElse(() => throw ServerFailure());
      expect(moviesModel.page, 1);
      expect(moviesModel.results?.length, 1);
      expect(moviesModel.results?[0].id, 123);
    });
  });
}
