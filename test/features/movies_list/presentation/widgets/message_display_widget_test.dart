import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:yologroup_assignment/presentation/movies_list/widgets/message_display_widget.dart';

void main() {
  testWidgets('verify Message Display widget displays the message passed',
      (WidgetTester tester) async {
    await tester.runAsync(() async {
      await tester.pumpWidget(const MaterialApp(
        home: MessageDisplay(
          message: 'Some error',
        ),
      ));

      expectLater(
          find.byKey(const ValueKey('message_display')), findsOneWidget);
      expectLater(find.text('Some error'), findsOneWidget);
    });
  });
}
