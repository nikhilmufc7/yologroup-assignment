import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:network_image_mock/network_image_mock.dart';
import 'package:yologroup_assignment/common/utils/enums.dart';
import 'package:yologroup_assignment/dependency_injection.dart';
import 'package:yologroup_assignment/presentation/movies_list/cubit/movies_cubit.dart';
import 'package:yologroup_assignment/data/movies_list/models/movies_data_model.dart';
import 'package:yologroup_assignment/presentation/movies_list/widgets/movie_poster_list_widget.dart';
import '../pages/movies_list_page_test.mocks.dart';

void main() {
  late MockMoviesCubit mockMoviesCubit;

  setUp(() {
    mockMoviesCubit = MockMoviesCubit();

    sl.registerFactory<MoviesCubit>(() => mockMoviesCubit);
  });

  tearDown(() {
    sl.reset();
  });

  testWidgets('DisplayLatestMoviesResultsWidget shows the correct movie type',
      (WidgetTester tester) async {
    const MovieType testMovieType = MovieType.latest;
    const String testMovieTypeName = 'Latest Movies';

    const moviesData = [
      MovieResults(id: 1),
      MovieResults(id: 2),
    ];

    await mockNetworkImagesFor(() => tester.pumpWidget(
          MaterialApp(
            home: Scaffold(
              body: MoviePosterListWidget(
                moviesData: moviesData,
                moviesCubit: mockMoviesCubit,
                movieType: testMovieType,
              ),
            ),
          ),
        ));

    final movieTypeFinder = find.text(testMovieTypeName);
    expect(movieTypeFinder, findsOneWidget);
  });
}
