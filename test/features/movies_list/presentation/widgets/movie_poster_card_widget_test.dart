import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:network_image_mock/network_image_mock.dart';
import 'package:yologroup_assignment/dependency_injection.dart';
import 'package:yologroup_assignment/presentation/movie_details/cubit/movie_details_cubit.dart';
import 'package:yologroup_assignment/presentation/movie_details/pages/movie_details_page.dart';
import 'package:yologroup_assignment/data/movies_list/models/movies_data_model.dart';
import 'package:yologroup_assignment/presentation/movies_list/cubit/movies_cubit.dart';
import 'package:yologroup_assignment/presentation/movies_list/widgets/movie_poster_card_widget.dart';

import '../pages/movies_list_page_test.mocks.dart';

void main() {
  late MockMoviesCubit mockMoviesCubit;
  late MovieDetailsCubit movieDetailsCubit;

  setUp(() {
    mockMoviesCubit = MockMoviesCubit();
    sl.registerFactory<MoviesCubit>(() => mockMoviesCubit);
    movieDetailsCubit = MockMovieDetailsCubit();
    sl.registerFactory<MovieDetailsCubit>(() => movieDetailsCubit);
  });

  tearDown(() {
    sl.reset();
  });
  testWidgets(
      'LatestMoviesResultsCard widget displays the provided movie details',
      (WidgetTester tester) async {
    const int testMovieId = 123;
    const String testMovieTitle = 'Spiderman';
    const String testMoviePosterPath = 'spiderman.jpg';

    const movieResults = MovieResults(
      id: testMovieId,
      title: testMovieTitle,
      poster_path: testMoviePosterPath,
    );

    await mockNetworkImagesFor(() => tester.pumpWidget(const MaterialApp(
          home: Scaffold(
            body: MoviePosterCardWidget(items: movieResults),
          ),
        )));

    final imageFinder = find.byType(Image);
    expect(imageFinder, findsOneWidget);

    final titleFinder = find.text(testMovieTitle);
    expect(titleFinder, findsOneWidget);

    await tester.tap(find.byType(InkWell));
    await tester.pumpAndSettle();

    final movieDetailsPageFinder = find.byType(MovieDetailsPage);
    expect(movieDetailsPageFinder, findsOneWidget);
  });
}
