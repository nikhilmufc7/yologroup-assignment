import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:yologroup_assignment/data/movies_list/models/movies_data_model.dart';
import 'package:yologroup_assignment/presentation/movies_list/cubit/movies_state.dart';
import 'package:yologroup_assignment/presentation/movies_list/widgets/loading_widget.dart';
import 'package:yologroup_assignment/presentation/movies_list/widgets/message_display_widget.dart';
import 'package:yologroup_assignment/presentation/movies_list/widgets/movie_poster_list_widget.dart';
import 'package:yologroup_assignment/presentation/movies_list/widgets/movie_type_card_widgets/latest_movies_card_widget.dart';
import '../pages/movies_list_page_test.mocks.dart';

void main() {
  late MockMoviesCubit mockMoviesCubit;

  setUp(() {
    mockMoviesCubit = MockMoviesCubit();
  });

  tearDown(() {
    reset(mockMoviesCubit);
  });

  testWidgets('LatestMoviesCardWidget shows loading state',
      (WidgetTester tester) async {
    when(mockMoviesCubit.state).thenReturn(LatestMoviesLoadingState());

    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: LatestMoviesCardWidget(moviesCubit: mockMoviesCubit),
        ),
      ),
    );

    expect(find.byType(LoadingWidget), findsOneWidget);
    expect(find.byKey(const Key('loading_widget_key')), findsOneWidget);
  });

  testWidgets('LatestMoviesCardWidget shows error state',
      (WidgetTester tester) async {
    when(mockMoviesCubit.state).thenReturn(
      LatestMoviesErrorState(message: 'Error loading movies'),
    );

    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: LatestMoviesCardWidget(moviesCubit: mockMoviesCubit),
        ),
      ),
    );

    expect(find.byType(MessageDisplay), findsOneWidget);
    expect(find.text('Error loading movies'), findsOneWidget);
    expect(find.byKey(const Key('message_display_widget_key_error')),
        findsOneWidget);
  });

  testWidgets('LatestMoviesCardWidget shows data state',
      (WidgetTester tester) async {
    when(mockMoviesCubit.state).thenReturn(
      LatestMoviesLoadedState(
        latestMovies: const MoviesModel(results: [
          MovieResults(id: 1),
          MovieResults(id: 2),
        ]),
      ),
    );

    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: LatestMoviesCardWidget(moviesCubit: mockMoviesCubit),
        ),
      ),
    );

    expect(find.byType(MoviePosterListWidget), findsOneWidget);
    expect(find.byKey(const Key('movies_card_key_widget')), findsOneWidget);
  });
}
