import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:yologroup_assignment/presentation/movies_list/widgets/loading_widget.dart';

void main() {
  testWidgets('verify loading widget is visible', (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(
      home: LoadingWidget(),
    ));

    expect(find.byKey(const ValueKey('loading_widget')), findsOneWidget);
  });
}
