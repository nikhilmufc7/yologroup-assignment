import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:yologroup_assignment/dependency_injection.dart';
import 'package:yologroup_assignment/presentation/movie_details/cubit/movie_details_cubit.dart';
import 'package:yologroup_assignment/presentation/movies_list/cubit/movies_cubit.dart';
import 'package:yologroup_assignment/presentation/movies_list/pages/movies_list_page.dart';
import 'movies_list_page_test.mocks.dart';

@GenerateNiceMocks([MockSpec<MoviesCubit>(), MockSpec<MovieDetailsCubit>()])
void main() {
  late MockMoviesCubit mockMoviesCubit;

  setUp(() {
    mockMoviesCubit = MockMoviesCubit();
    sl.registerFactory<MoviesCubit>(() => mockMoviesCubit);
  });

  tearDown(() {
    sl.reset();
  });

  testWidgets('MoviesListPage builds correctly', (WidgetTester tester) async {
    await tester.pumpWidget(
      const MaterialApp(
        home: MoviesListPage(),
      ),
    );

    expect(find.byKey(const ValueKey('latest_movies_card_key_widget')),
        findsOneWidget);

    expect(find.byKey(const ValueKey('popular_movies_card_key_widget')),
        findsOneWidget);

    expect(find.byKey(const ValueKey('top_rated_movies_card_key_widget')),
        findsOneWidget);
  });
}
