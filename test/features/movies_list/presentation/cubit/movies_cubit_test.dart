import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:dartz/dartz.dart';
import 'package:yologroup_assignment/common/constants/constants.dart';
import 'package:yologroup_assignment/common/usecase/params.dart';
import 'package:yologroup_assignment/data/movies_list/models/movies_data_model.dart';
import 'package:yologroup_assignment/domain/movies_list/usecase/get_all_latest_movies_usecase.dart';
import 'package:yologroup_assignment/domain/movies_list/usecase/get_all_popular_movies_usecase.dart';
import 'package:yologroup_assignment/domain/movies_list/usecase/get_all_top_rated_movies_usecase.dart';
import 'package:yologroup_assignment/domain/movies_list/usecase/get_all_upcoming_movies_usecase.dart';
import 'package:yologroup_assignment/presentation/movies_list/cubit/movies_cubit.dart';
import 'package:yologroup_assignment/presentation/movies_list/cubit/movies_state.dart';

class MockGetAllLatestMoviesUsecase extends Mock
    implements GetAllLatestMoviesUsecase {}

class MockGetAllPopularMoviesUsecase extends Mock
    implements GetAllPopularMoviesUsecase {}

class MockGetAllTopRatedMoviesUsecase extends Mock
    implements GetAllTopRatedMoviesUsecase {}

class MockGetAllUpcomingMoviesUsecase extends Mock
    implements GetAllUpcomingMoviesUsecase {}

void main() {
  late MoviesCubit moviesCubit;
  late GetAllLatestMoviesUsecase getAllLatestMoviesUsecase;
  late GetAllPopularMoviesUsecase getAllPopularMoviesUsecase;
  late GetAllTopRatedMoviesUsecase getAllTopRatedMoviesUsecase;
  late GetAllUpcomingMoviesUsecase getAllUpcomingMoviesUsecase;

  setUp(() {
    getAllLatestMoviesUsecase = MockGetAllLatestMoviesUsecase();
    getAllPopularMoviesUsecase = MockGetAllPopularMoviesUsecase();
    getAllTopRatedMoviesUsecase = MockGetAllTopRatedMoviesUsecase();
    getAllUpcomingMoviesUsecase = MockGetAllUpcomingMoviesUsecase();

    moviesCubit = MoviesCubit(
      getAllLatestMoviesUsecase,
      getAllPopularMoviesUsecase,
      getAllTopRatedMoviesUsecase,
      getAllUpcomingMoviesUsecase,
    );
  });

  tearDown(() {
    moviesCubit.close();
  });

  group('MoviesCubit', () {
    test('emits LatestMoviesLoadedState when getLatestMovies is called',
        () async {
      const moviesModel = MoviesModel(
        page: 1,
        results: [
          MovieResults(id: 123),
        ],
      );

      when(() => getAllLatestMoviesUsecase
          .call(const Params(1, Constants.latestMoviesPath))).thenAnswer(
        (_) async => const Right(moviesModel),
      );

      expect(
        moviesCubit.stream,
        emitsInOrder([
          LatestMoviesLoadingState(),
          LatestMoviesLoadedState(latestMovies: moviesModel),
        ]),
      );

      await moviesCubit.getLatestMovies();

      verify(() => getAllLatestMoviesUsecase
          .call(const Params(1, Constants.latestMoviesPath))).called(1);
    });

    test('emits PopularMoviesLoadedState when getPopularMovies is called',
        () async {
      const moviesModel = MoviesModel(
        page: 1,
        results: [
          MovieResults(id: 456),
        ],
      );

      when(() => getAllPopularMoviesUsecase
          .call(const Params(1, Constants.popularMoviesPath))).thenAnswer(
        (_) async => const Right(moviesModel),
      );

      expect(
        moviesCubit.stream,
        emitsInOrder([
          PopularMoviesLoadingState(),
          PopularMoviesLoadedState(latestMovies: moviesModel),
        ]),
      );

      await moviesCubit.getPopularMovies();

      verify(() => getAllPopularMoviesUsecase
          .call(const Params(1, Constants.popularMoviesPath))).called(1);
    });

    test('emits TopRatedMoviesLoadedState when getTopRatedMovies is called',
        () async {
      const moviesModel = MoviesModel(
        page: 1,
        results: [
          MovieResults(id: 789),
        ],
      );

      when(() => getAllTopRatedMoviesUsecase
          .call(const Params(1, Constants.topRatesMoviesPath))).thenAnswer(
        (_) async => const Right(moviesModel),
      );

      expect(
        moviesCubit.stream,
        emitsInOrder([
          TopRatedMoviesLoadingState(),
          TopRatedMoviesLoadedState(latestMovies: moviesModel),
        ]),
      );

      await moviesCubit.getTopRatedMovies();

      verify(() => getAllTopRatedMoviesUsecase
          .call(const Params(1, Constants.topRatesMoviesPath))).called(1);
    });

    test('emits UpcomingMoviesLoadedState when getUpcomingMovies is called',
        () async {
      const moviesModel = MoviesModel(
        page: 1,
        results: [
          MovieResults(id: 987),
        ],
      );

      when(() => getAllUpcomingMoviesUsecase
          .call(const Params(1, Constants.upcomingMoviesPath))).thenAnswer(
        (_) async => const Right(moviesModel),
      );

      expect(
        moviesCubit.stream,
        emitsInOrder([
          UpcomingMoviesLoadingState(),
          UpcomingMoviesLoadedState(moviesData: moviesModel),
        ]),
      );

      await moviesCubit.getUpcomingMovies();

      verify(() => getAllUpcomingMoviesUsecase
          .call(const Params(1, Constants.upcomingMoviesPath))).called(1);
    });
  });
}
