import 'package:dartz/dartz.dart';
import '../../../common/constants/constants.dart';
import '../../../common/error/failures.dart';
import '../../../common/usecase/params.dart';
import '../../../common/usecase/usecase.dart';
import '../../../data/movies_list/models/movies_data_model.dart';
import '../repository/movies_repository.dart';

class GetAllUpcomingMoviesUsecase implements UseCase<MoviesModel, Params> {
  final MoviesRepository repository;

  GetAllUpcomingMoviesUsecase(this.repository);

  @override
  Future<Either<Failure, MoviesModel>> call(Params params) async {
    return await repository.getMoviesRepositories(
        params.page, Constants.apiKey, params.path);
  }
}
