import 'package:dartz/dartz.dart';
import 'package:yologroup_assignment/common/constants/constants.dart';
import 'package:yologroup_assignment/common/error/failures.dart';
import 'package:yologroup_assignment/common/usecase/usecase.dart';
import 'package:yologroup_assignment/domain/movies_list/repository/movies_repository.dart';

import '../../../common/usecase/params.dart';
import '../../../data/movies_list/models/movies_data_model.dart';

class GetAllLatestMoviesUsecase implements UseCase<MoviesModel, Params> {
  final MoviesRepository repository;

  GetAllLatestMoviesUsecase(this.repository);

  @override
  Future<Either<Failure, MoviesModel>> call(Params params) async {
    return await repository.getMoviesRepositories(
        params.page, Constants.apiKey, params.path);
  }
}
