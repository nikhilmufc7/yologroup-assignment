import 'package:dartz/dartz.dart';
import 'package:yologroup_assignment/common/error/failures.dart';
import '../../../data/movies_list/models/movies_data_model.dart';

abstract class MoviesRepository {
  Future<Either<Failure, MoviesModel>> getMoviesRepositories(
      int page, String apiKey, String path);
}
