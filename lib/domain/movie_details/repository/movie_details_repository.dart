import 'package:dartz/dartz.dart';
import 'package:yologroup_assignment/common/error/failures.dart';
import 'package:yologroup_assignment/data/movie_details/models/movie_details_model.dart';

abstract class MovieDetailsRepository {
  Future<Either<Failure, MovieDetailsModel>> getMovieDetailsRepositories(
      int movieId, String apiKey);
}
