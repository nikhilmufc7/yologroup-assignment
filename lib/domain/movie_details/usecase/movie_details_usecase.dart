import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:yologroup_assignment/common/constants/constants.dart';
import 'package:yologroup_assignment/common/error/failures.dart';
import 'package:yologroup_assignment/common/usecase/usecase.dart';
import 'package:yologroup_assignment/data/movie_details/models/movie_details_model.dart';
import 'package:yologroup_assignment/domain/movie_details/repository/movie_details_repository.dart';

class GetMovieDetailsUsecase implements UseCase<MovieDetailsModel, Params> {
  final MovieDetailsRepository repository;

  GetMovieDetailsUsecase(this.repository);

  @override
  Future<Either<Failure, MovieDetailsModel>> call(Params params) async {
    return await repository.getMovieDetailsRepositories(
        params.movieId, Constants.apiKey);
  }
}

class Params extends Equatable {
  final int movieId;

  const Params(this.movieId);

  @override
  List<Object> get props => [];
}
