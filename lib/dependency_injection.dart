import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:yologroup_assignment/common/constants/constants.dart';
import 'package:yologroup_assignment/data/movie_details/endpoint/movie_details_endpoint.dart';
import 'package:yologroup_assignment/data/movie_details/repository/movie_details_repository_impl.dart';
import 'package:yologroup_assignment/domain/movies_list/usecase/get_all_latest_movies_usecase.dart';
import 'package:yologroup_assignment/domain/movie_details/repository/movie_details_repository.dart';
import 'package:yologroup_assignment/domain/movie_details/usecase/movie_details_usecase.dart';
import 'package:yologroup_assignment/presentation/movies_list/cubit/movies_cubit.dart';
import 'package:yologroup_assignment/presentation/movie_details/cubit/movie_details_cubit.dart';

import 'data/movies_list/endpoint/movies_list_endpoint.dart';
import 'data/movies_list/repository/movies_list_repo_impl.dart';
import 'domain/movies_list/repository/movies_repository.dart';
import 'domain/movies_list/usecase/get_all_popular_movies_usecase.dart';
import 'domain/movies_list/usecase/get_all_top_rated_movies_usecase.dart';
import 'domain/movies_list/usecase/get_all_upcoming_movies_usecase.dart';

final sl = GetIt.instance;

Future<void> init() async {
  final Dio dioClient = Dio(
    BaseOptions(
      baseUrl: Constants.apiUrl,
      connectTimeout: const Duration(
        milliseconds: 5000,
      ),
      receiveTimeout: const Duration(
        milliseconds: 5000,
      ),
      responseType: ResponseType.json,
    ),
  );

  sl.registerFactory<MoviesCubit>(
    () => MoviesCubit(
      sl(),
      sl(),
      sl(),
      sl(),
    ),
  );

  sl.registerFactory<MovieDetailsCubit>(
    () => MovieDetailsCubit(
      sl(),
    ),
  );

  // Use cases
  sl.registerLazySingleton<GetAllLatestMoviesUsecase>(
      () => GetAllLatestMoviesUsecase(sl()));

  sl.registerLazySingleton<GetAllPopularMoviesUsecase>(
      () => GetAllPopularMoviesUsecase(sl()));

  sl.registerLazySingleton<GetAllTopRatedMoviesUsecase>(
      () => GetAllTopRatedMoviesUsecase(sl()));

  sl.registerLazySingleton<GetAllUpcomingMoviesUsecase>(
      () => GetAllUpcomingMoviesUsecase(sl()));

  sl.registerLazySingleton<GetMovieDetailsUsecase>(
      () => GetMovieDetailsUsecase(sl()));

  // Repository
  sl.registerLazySingleton<MoviesRepository>(
    () => MoviesListRepoImpl(
      moviesListEndpoint: sl(),
    ),
  );

  sl.registerLazySingleton<MovieDetailsRepository>(
    () => MovieDetailsRepositoryImpl(
      movieDetailsApiEndpoints: sl(),
    ),
  );

  // Data sources
  sl.registerLazySingleton<MoviesListEndpoint>(
    () => MoviesListEndpoint(dioClient),
  );

  sl.registerLazySingleton<MovieDetailsEndpoint>(
    () => MovieDetailsEndpoint(dioClient),
  );
}
