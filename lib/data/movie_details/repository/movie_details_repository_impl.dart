import 'package:dartz/dartz.dart';
import 'package:yologroup_assignment/common/error/exceptions.dart';
import 'package:yologroup_assignment/common/error/failures.dart';
import 'package:yologroup_assignment/data/movie_details/models/movie_details_model.dart';
import 'package:yologroup_assignment/domain/movie_details/repository/movie_details_repository.dart';
import '../endpoint/movie_details_endpoint.dart';

class MovieDetailsRepositoryImpl implements MovieDetailsRepository {
  final MovieDetailsEndpoint movieDetailsApiEndpoints;

  MovieDetailsRepositoryImpl({
    required this.movieDetailsApiEndpoints,
  });

  @override
  Future<Either<Failure, MovieDetailsModel>> getMovieDetailsRepositories(
      int movieId, String apiKey) async {
    try {
      var response =
          await movieDetailsApiEndpoints.getMovieDetails(movieId, apiKey);
      return Right(response);
    } on ServerException {
      return Left(ServerFailure());
    }
  }
}
