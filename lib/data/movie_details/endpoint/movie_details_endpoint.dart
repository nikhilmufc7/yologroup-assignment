import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'package:yologroup_assignment/common/constants/constants.dart';
import '../models/movie_details_model.dart';
part 'movie_details_endpoint.g.dart';

@RestApi(baseUrl: Constants.apiUrl)
abstract class MovieDetailsEndpoint {
  factory MovieDetailsEndpoint(Dio dio) = _MovieDetailsEndpoint;

  @GET("{id}")
  Future<MovieDetailsModel> getMovieDetails(
    @Path("id") int id,
    @Query("api_key") String apiKey,
  );
}
