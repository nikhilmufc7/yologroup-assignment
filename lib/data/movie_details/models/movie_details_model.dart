// ignore_for_file: non_constant_identifier_names

import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
part 'movie_details_model.g.dart';

@JsonSerializable()
class MovieDetailsModel extends Equatable {
  final bool? adult;
  final String? backdrop_path;
  final int? budget;
  final List<Genres>? genres;
  final String? homepage;
  final int? id;
  final String? imdb_id;
  final String? original_language;
  final String? original_title;
  final String? overview;
  final double? popularity;
  final String? poster_path;
  final List<ProductionCompanies>? production_companies;
  final List<ProductionCountries>? production_countries;
  final String? release_date;
  final int? revenue;
  final int? runtime;
  final List<SpokenLanguages>? spoken_languages;
  final String? status;
  final String? tagline;
  final String? title;
  final bool? video;
  final num? vote_average;
  final int? vote_count;

  const MovieDetailsModel(
      {this.adult,
      this.backdrop_path,
      this.budget,
      this.genres,
      this.homepage,
      this.id,
      this.imdb_id,
      this.original_language,
      this.original_title,
      this.overview,
      this.popularity,
      this.poster_path,
      this.production_companies,
      this.production_countries,
      this.release_date,
      this.revenue,
      this.runtime,
      this.spoken_languages,
      this.status,
      this.tagline,
      this.title,
      this.video,
      this.vote_average,
      this.vote_count});

  factory MovieDetailsModel.fromJson(Map<String, dynamic> json) =>
      _$MovieDetailsModelFromJson(json);

  @override
  List<Object?> get props => [
        id,
      ];

  Map<String, dynamic> toJson() => _$MovieDetailsModelToJson(this);
}

@JsonSerializable()
class Genres extends Equatable {
  final int? id;
  final String? name;

  const Genres({this.id, this.name});

  factory Genres.fromJson(Map<String, dynamic> json) => _$GenresFromJson(json);

  @override
  List<Object?> get props => [
        id,
      ];

  Map<String, dynamic> toJson() => _$GenresToJson(this);
}

@JsonSerializable()
class ProductionCompanies extends Equatable {
  final int? id;
  final String? logo_path;
  final String? name;
  final String? origin_country;

  const ProductionCompanies(
      {this.id, this.logo_path, this.name, this.origin_country});

  factory ProductionCompanies.fromJson(Map<String, dynamic> json) =>
      _$ProductionCompaniesFromJson(json);

  @override
  List<Object?> get props => [
        id,
      ];

  Map<String, dynamic> toJson() => _$ProductionCompaniesToJson(this);
}

@JsonSerializable()
class ProductionCountries extends Equatable {
  final String? iso31661;
  final String? name;

  const ProductionCountries({this.iso31661, this.name});

  factory ProductionCountries.fromJson(Map<String, dynamic> json) =>
      _$ProductionCountriesFromJson(json);

  @override
  List<Object?> get props => [
        iso31661,
      ];

  Map<String, dynamic> toJson() => _$ProductionCountriesToJson(this);
}

@JsonSerializable()
class SpokenLanguages extends Equatable {
  final String? english_name;
  final String? iso6391;
  final String? name;

  const SpokenLanguages({this.english_name, this.iso6391, this.name});

  factory SpokenLanguages.fromJson(Map<String, dynamic> json) =>
      _$SpokenLanguagesFromJson(json);

  @override
  List<Object?> get props => [
        iso6391,
      ];

  Map<String, dynamic> toJson() => _$SpokenLanguagesToJson(this);
}
