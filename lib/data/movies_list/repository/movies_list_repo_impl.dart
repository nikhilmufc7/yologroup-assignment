import 'package:dartz/dartz.dart';
import 'package:yologroup_assignment/common/error/exceptions.dart';
import 'package:yologroup_assignment/common/error/failures.dart';
import 'package:yologroup_assignment/domain/movies_list/repository/movies_repository.dart';
import '../endpoint/movies_list_endpoint.dart';
import '../models/movies_data_model.dart';

class MoviesListRepoImpl implements MoviesRepository {
  final MoviesListEndpoint moviesListEndpoint;

  MoviesListRepoImpl({
    required this.moviesListEndpoint,
  });

  @override
  Future<Either<Failure, MoviesModel>> getMoviesRepositories(
      int page, String apiKey, String path) async {
    try {
      var response = await moviesListEndpoint.getMoviesList(path, apiKey, page);
      return Right(response);
    } on ServerException {
      return Left(ServerFailure());
    }
  }
}
