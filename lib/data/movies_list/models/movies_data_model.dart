// ignore_for_file: non_constant_identifier_names

import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
part 'movies_data_model.g.dart';

@JsonSerializable()
class MoviesModel extends Equatable {
  final Dates? dates;
  final int? page;
  final List<MovieResults>? results;
  final int? total_pages;
  final int? total_results;

  const MoviesModel(
      {this.dates,
      this.page,
      this.results,
      this.total_pages,
      this.total_results});

  factory MoviesModel.fromJson(Map<String, dynamic> json) =>
      _$MoviesModelFromJson(json);

  @override
  List<Object?> get props => [
        results,
      ];

  Map<String, dynamic> toJson() => _$MoviesModelToJson(this);
}

@JsonSerializable()
class Dates extends Equatable {
  final String? maximum;
  final String? minimum;

  const Dates({this.maximum, this.minimum});

  factory Dates.fromJson(Map<String, dynamic> json) => _$DatesFromJson(json);

  @override
  List<Object?> get props => [
        minimum,
        maximum,
      ];

  Map<String, dynamic> toJson() => _$DatesToJson(this);
}

@JsonSerializable()
class MovieResults extends Equatable {
  final bool? adult;
  final String? backdrop_path;
  final List<int>? genre_ids;
  final int? id;
  final String? original_language;
  final String? original_title;
  final String? overview;
  final double? popularity;
  final String? poster_path;
  final String? release_date;
  final String? title;
  final bool? video;
  final num? vote_average;
  final int? vote_count;

  const MovieResults(
      {this.adult,
      this.backdrop_path,
      this.genre_ids,
      this.id,
      this.original_language,
      this.original_title,
      this.overview,
      this.popularity,
      this.poster_path,
      this.release_date,
      this.title,
      this.video,
      this.vote_average,
      this.vote_count});

  factory MovieResults.fromJson(Map<String, dynamic> json) =>
      _$MovieResultsFromJson(json);

  @override
  List<Object?> get props => [
        id,
      ];

  Map<String, dynamic> toJson() => _$MovieResultsToJson(this);
}
