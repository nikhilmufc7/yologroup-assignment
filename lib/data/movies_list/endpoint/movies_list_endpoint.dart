import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';

import '../models/movies_data_model.dart';
part 'movies_list_endpoint.g.dart';

@RestApi()
abstract class MoviesListEndpoint {
  factory MoviesListEndpoint(Dio dio) = _MoviesListEndpoint;

  @GET("{path}")
  Future<MoviesModel> getMoviesList(
    @Path("path") String path,
    @Query("api_key") String apiKey,
    @Query("page") int page,
  );
}
