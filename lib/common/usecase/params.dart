import 'package:equatable/equatable.dart';

class Params extends Equatable {
  final int page;
  final String path;

  const Params(this.page, this.path);

  @override
  List<Object> get props => [page, path];
}
