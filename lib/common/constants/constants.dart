class Constants {
  static const String apiUrl = "https://api.themoviedb.org/3/movie/";
  static const latestMoviesPath = "now_playing";
  static const popularMoviesPath = "popular";
  static const topRatesMoviesPath = "top_rated";
  static const upcomingMoviesPath = "upcoming";
  static const apiKey = "22cda16953876cd5324e482696d8a60c";
  static const topImageUrl =
      "https://cdn.mos.cms.futurecdn.net/QFuzEB2Gxth6qQT8Tpe7LN.jpg";
  static const movieBannerPath = "https://image.tmdb.org/t/p/original/";
}
