import 'package:flutter/material.dart';

class CommonSnackBar {
  final String message;

  const CommonSnackBar({
    required this.message,
  });

  static void show(
    BuildContext context,
    String message,
  ) {
    ScaffoldMessenger.maybeOf(context)?.showSnackBar(
      SnackBar(
        elevation: 0.0,
        content: Text(message),
        duration: const Duration(seconds: 2),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(16.0),
            topRight: Radius.circular(16.0),
          ),
        ),
      ),
    );
  }
}
