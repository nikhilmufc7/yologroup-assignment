import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:yologroup_assignment/common/constants/constants.dart';
import 'package:yologroup_assignment/dependency_injection.dart';
import 'package:yologroup_assignment/presentation/movies_list/cubit/movies_cubit.dart';
import 'package:yologroup_assignment/presentation/movies_list/pages/movies_list_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider(create: (_) => sl<MoviesCubit>()),
        ],
        child: Scaffold(
          backgroundColor: Colors.grey.withOpacity(0.2),
          appBar: AppBar(
            backgroundColor: Colors.black,
            title: const Text('Yolo Group Assignment'),
          ),
          body: SingleChildScrollView(
            child: Column(
              children: [
                Image.network(
                  Constants.topImageUrl,
                  key: const ValueKey('top_image_url_key'),
                ),
                const MoviesListPage(
                  key: ValueKey('movies_list_page_key'),
                ),
              ],
            ),
          ),
        ));
  }
}
