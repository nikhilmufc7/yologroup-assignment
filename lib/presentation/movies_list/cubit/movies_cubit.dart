import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:yologroup_assignment/common/constants/constants.dart';
import 'package:yologroup_assignment/data/movies_list/models/movies_data_model.dart';
import 'package:yologroup_assignment/domain/movies_list/usecase/get_all_latest_movies_usecase.dart';
import 'package:yologroup_assignment/domain/movies_list/usecase/get_all_popular_movies_usecase.dart';
import 'package:yologroup_assignment/domain/movies_list/usecase/get_all_top_rated_movies_usecase.dart';
import 'package:yologroup_assignment/domain/movies_list/usecase/get_all_upcoming_movies_usecase.dart';
import '../../../common/usecase/params.dart';
import 'movies_state.dart';

class MoviesCubit extends Cubit<MoviesState> {
  final GetAllLatestMoviesUsecase getAlllatestMoviesUsecase;
  final GetAllPopularMoviesUsecase getAllPopularMoviesUsecase;
  final GetAllTopRatedMoviesUsecase getAllTopRatedMoviesUsecase;
  final GetAllUpcomingMoviesUsecase getAllUpcomingMoviesUsecase;

  MoviesCubit(this.getAlllatestMoviesUsecase, this.getAllPopularMoviesUsecase,
      this.getAllTopRatedMoviesUsecase, this.getAllUpcomingMoviesUsecase)
      : super(LatestMoviesInitialState());

  int latestMoviesPage = 1;
  int popularMoviesPage = 1;
  int topRatedMoviesPage = 1;
  int upcomingMoviesPage = 1;

  List<MovieResults> latestMoviesList = [];
  List<MovieResults> popularMoviesList = [];
  List<MovieResults> topRatedMoviesList = [];
  List<MovieResults> upcomingMoviesList = [];

  final ValueNotifier<bool> isUpcomingExpanded = ValueNotifier<bool>(false);
  final ValueNotifier<bool> isTopRatedExpanded = ValueNotifier<bool>(false);
  final ValueNotifier<bool> isLatestExpanded = ValueNotifier<bool>(true);
  final ValueNotifier<bool> isPopularExpanded = ValueNotifier<bool>(true);

  Future<void> getLatestMovies() async {
    emit(LatestMoviesLoadingState());

    try {
      final result = await getAlllatestMoviesUsecase
          .call(Params(latestMoviesPage, Constants.latestMoviesPath));

      result.fold(
          (failure) =>
              emit(LatestMoviesErrorState(message: failure.toString())),
          (repositoriesData) {
        latestMoviesPage++;
        if (repositoriesData.results != null) {
          latestMoviesList += repositoriesData.results!;
        }
        emit(LatestMoviesLoadedState(latestMovies: repositoriesData));
      });
    } catch (e) {
      emit(LatestMoviesErrorState(message: e.toString()));
    }
  }

  Future<void> getPopularMovies() async {
    emit(PopularMoviesLoadingState());

    try {
      final result = await getAllPopularMoviesUsecase
          .call(Params(popularMoviesPage, Constants.popularMoviesPath));

      result.fold(
          (failure) =>
              emit(PopularMoviesErrorState(message: failure.toString())),
          (repositoriesData) {
        popularMoviesPage++;
        if (repositoriesData.results != null) {
          popularMoviesList += repositoriesData.results!;
        }
        emit(PopularMoviesLoadedState(latestMovies: repositoriesData));
      });
    } catch (e) {
      emit(PopularMoviesErrorState(message: e.toString()));
    }
  }

  Future<void> getTopRatedMovies() async {
    emit(TopRatedMoviesLoadingState());

    try {
      final result = await getAllTopRatedMoviesUsecase
          .call(Params(topRatedMoviesPage, Constants.topRatesMoviesPath));

      result.fold(
          (failure) =>
              emit(TopRatedMoviesErrorState(message: failure.toString())),
          (repositoriesData) {
        topRatedMoviesPage++;
        if (repositoriesData.results != null) {
          topRatedMoviesList += repositoriesData.results!;
        }
        emit(TopRatedMoviesLoadedState(latestMovies: repositoriesData));
      });
    } catch (e) {
      emit(TopRatedMoviesErrorState(message: e.toString()));
    }
  }

  Future<void> getUpcomingMovies() async {
    emit(UpcomingMoviesLoadingState());

    try {
      final result = await getAllUpcomingMoviesUsecase
          .call(Params(upcomingMoviesPage, Constants.upcomingMoviesPath));

      result.fold(
          (failure) =>
              emit(UpcomingMoviesErrorState(message: failure.toString())),
          (repositoriesData) {
        upcomingMoviesPage++;
        if (repositoriesData.results != null) {
          upcomingMoviesList += repositoriesData.results!;
        }
        emit(UpcomingMoviesLoadedState(moviesData: repositoriesData));
      });
    } catch (e) {
      emit(UpcomingMoviesErrorState(message: e.toString()));
    }
  }
}
