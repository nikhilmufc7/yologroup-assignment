import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../../data/movies_list/models/movies_data_model.dart';

@immutable
abstract class MoviesState extends Equatable {
  @override
  List<Object> get props => [];
}

class LatestMoviesInitialState extends MoviesState {}

class LatestMoviesEmptyState extends MoviesState {}

class LatestMoviesLoadingState extends MoviesState {}

class LatestMoviesLoadedState extends MoviesState {
  final MoviesModel latestMovies;

  LatestMoviesLoadedState({required this.latestMovies});

  @override
  List<Object> get props => [latestMovies];
}

class LatestMoviesErrorState extends MoviesState {
  final String message;

  LatestMoviesErrorState({required this.message});

  @override
  List<Object> get props => [message];
}

class PopularMoviesInitialState extends MoviesState {}

class PopularMoviesEmptyState extends MoviesState {}

class PopularMoviesLoadingState extends MoviesState {}

class PopularMoviesLoadedState extends MoviesState {
  final MoviesModel latestMovies;

  PopularMoviesLoadedState({required this.latestMovies});

  @override
  List<Object> get props => [latestMovies];
}

class PopularMoviesErrorState extends MoviesState {
  final String message;

  PopularMoviesErrorState({required this.message});

  @override
  List<Object> get props => [message];
}

class TopRatedMoviesInitialState extends MoviesState {}

class TopRatedMoviesEmptyState extends MoviesState {}

class TopRatedMoviesLoadingState extends MoviesState {}

class TopRatedMoviesLoadedState extends MoviesState {
  final MoviesModel latestMovies;

  TopRatedMoviesLoadedState({required this.latestMovies});

  @override
  List<Object> get props => [latestMovies];
}

class TopRatedMoviesErrorState extends MoviesState {
  final String message;

  TopRatedMoviesErrorState({required this.message});

  @override
  List<Object> get props => [message];
}

class UpcomingMoviesInitialState extends MoviesState {}

class UpcomingMoviesEmptyState extends MoviesState {}

class UpcomingMoviesLoadingState extends MoviesState {}

class UpcomingMoviesLoadedState extends MoviesState {
  final MoviesModel moviesData;

  UpcomingMoviesLoadedState({required this.moviesData});

  @override
  List<Object> get props => [moviesData];
}

class UpcomingMoviesErrorState extends MoviesState {
  final String message;

  UpcomingMoviesErrorState({required this.message});

  @override
  List<Object> get props => [message];
}
