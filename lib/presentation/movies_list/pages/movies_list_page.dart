import 'package:flutter/material.dart';
import 'package:yologroup_assignment/dependency_injection.dart';
import 'package:yologroup_assignment/presentation/movies_list/cubit/movies_cubit.dart';
import 'package:yologroup_assignment/presentation/movies_list/widgets/movie_type_card_widgets/latest_movies_card_widget.dart';
import 'package:yologroup_assignment/presentation/movies_list/widgets/movie_type_card_widgets/popular_movies_card_widget.dart';
import 'package:yologroup_assignment/presentation/movies_list/widgets/movie_type_card_widgets/top_rated_movies_card_widget.dart';
import 'package:yologroup_assignment/presentation/movies_list/widgets/movie_type_card_widgets/upcoming_movies_card_widget.dart';

class MoviesListPage extends StatefulWidget {
  const MoviesListPage({
    Key? key,
  }) : super(key: key);

  @override
  State<MoviesListPage> createState() => _MoviesListPageState();
}

class _MoviesListPageState extends State<MoviesListPage> {
  late MoviesCubit _moviesCubit;

  @override
  void initState() {
    super.initState();
    _moviesCubit = sl.get();
    _moviesCubit.getLatestMovies();
    _moviesCubit.getPopularMovies();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        LatestMoviesCardWidget(
          moviesCubit: _moviesCubit,
          key: const ValueKey('latest_movies_card_key_widget'),
        ),
        PopularMoviesCardWidget(
          moviesCubit: _moviesCubit,
          key: const ValueKey('popular_movies_card_key_widget'),
        ),
        TopRatedMoviesCardWidget(
          moviesCubit: _moviesCubit,
          key: const ValueKey('top_rated_movies_card_key_widget'),
        ),
        UpcomingMoviesCardWidget(
          moviesCubit: _moviesCubit,
          key: const ValueKey('upcoming_movies_card_key_widget'),
        )
      ],
    );
  }
}
