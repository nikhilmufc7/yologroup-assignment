import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:yologroup_assignment/common/utils/enums.dart';
import 'package:yologroup_assignment/common/utils/string_extension.dart';
import 'package:yologroup_assignment/dependency_injection.dart';
import 'package:yologroup_assignment/presentation/movies_list/cubit/movies_cubit.dart';
import 'package:yologroup_assignment/presentation/movies_list/widgets/movie_poster_card_widget.dart';

import '../../../data/movies_list/models/movies_data_model.dart';

class MoviePosterListWidget extends StatefulWidget {
  final List<MovieResults>? moviesData;
  final MoviesCubit moviesCubit;
  final MovieType movieType;

  const MoviePosterListWidget({
    Key? key,
    this.moviesData,
    required this.moviesCubit,
    required this.movieType,
  }) : super(key: key);

  @override
  State<MoviePosterListWidget> createState() => _MoviePosterListWidgetState();
}

class _MoviePosterListWidgetState extends State<MoviePosterListWidget> {
  late Timer _timer;

  final PagingController<int, MovieResults> _pagingController =
      PagingController(firstPageKey: 0);

  @override
  void initState() {
    super.initState();

    addDataToRespectiveController();

    _timer = Timer.periodic(const Duration(seconds: 30), (_) {
      _getMoviesDataBasedOnType(widget.movieType);
    });
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => sl<MoviesCubit>(),
      child: Theme(
        data: ThemeData().copyWith(dividerColor: Colors.transparent),
        child: ExpansionTile(
          initiallyExpanded: widget.moviesData != null,
          tilePadding: const EdgeInsets.symmetric(horizontal: 10),
          onExpansionChanged: onExpansionTypeChanged,
          maintainState: true,
          title: Text(
            "${widget.movieType.name.capitalize()} Movies",
            style: const TextStyle(
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Colors.black),
          ),
          children: <Widget>[
            SizedBox(
              height: MediaQuery.of(context).size.height / 3.6,
              child: PagedListView<int, MovieResults>(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                pagingController: _pagingController,
                builderDelegate: PagedChildBuilderDelegate<MovieResults>(
                    newPageProgressIndicatorBuilder: (context) =>
                        const SizedBox.shrink(),
                    itemBuilder: (context, item, index) {
                      return MoviePosterCardWidget(
                        items: item,
                        key: const ValueKey(
                            'latest_movies_results_card_key_widget'),
                      );
                    }),
              ),
            ),
          ],
        ),
      ),
    );
  }

  onExpansionTypeChanged(bool value) {
    switch (widget.movieType) {
      case MovieType.topRated:
        widget.moviesCubit.isTopRatedExpanded.value = value;
        if (value && widget.moviesCubit.topRatedMoviesList.isEmpty) {
          _getMoviesDataBasedOnType(MovieType.topRated);
        }
        break;
      case MovieType.upcoming:
        widget.moviesCubit.isUpcomingExpanded.value = value;
        if (value && widget.moviesCubit.upcomingMoviesList.isEmpty) {
          _getMoviesDataBasedOnType(MovieType.upcoming);
        }
        break;
      case MovieType.latest:
        widget.moviesCubit.isLatestExpanded.value = value;
        break;
      case MovieType.popular:
        widget.moviesCubit.isPopularExpanded.value = value;
        break;
    }
  }

  Future<void> _getMoviesDataBasedOnType(MovieType movieType) async {
    try {
      switch (movieType) {
        case MovieType.topRated:
          widget.moviesCubit.getTopRatedMovies();
          _pagingController.appendPage(widget.moviesCubit.topRatedMoviesList,
              widget.moviesCubit.topRatedMoviesPage);
          break;
        case MovieType.upcoming:
          widget.moviesCubit.getUpcomingMovies();
          _pagingController.appendPage(widget.moviesCubit.upcomingMoviesList,
              widget.moviesCubit.upcomingMoviesPage);
          break;
        case MovieType.latest:
          widget.moviesCubit.getLatestMovies();
          _pagingController.appendPage(widget.moviesCubit.latestMoviesList,
              widget.moviesCubit.latestMoviesPage);
          break;
        case MovieType.popular:
          widget.moviesCubit.getPopularMovies();
          _pagingController.appendPage(widget.moviesCubit.popularMoviesList,
              widget.moviesCubit.popularMoviesPage);
          break;
      }
    } catch (error) {
      _pagingController.error = error;
    }
  }

  addDataToRespectiveController() {
    switch (widget.movieType) {
      case MovieType.latest:
        _pagingController.appendPage(widget.moviesCubit.latestMoviesList,
            widget.moviesCubit.latestMoviesPage);
        break;
      case MovieType.popular:
        _pagingController.appendPage(widget.moviesCubit.popularMoviesList,
            widget.moviesCubit.popularMoviesPage);
        break;
      case MovieType.topRated:
        _pagingController.appendPage(widget.moviesCubit.topRatedMoviesList,
            widget.moviesCubit.topRatedMoviesPage);
        break;
      case MovieType.upcoming:
        _pagingController.appendPage(widget.moviesCubit.upcomingMoviesList,
            widget.moviesCubit.upcomingMoviesPage);
        break;
    }
  }
}
