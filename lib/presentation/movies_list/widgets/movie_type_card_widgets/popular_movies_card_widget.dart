import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:yologroup_assignment/presentation/movies_list/cubit/movies_cubit.dart';

import '../../../../common/utils/enums.dart';
import '../../cubit/movies_state.dart';
import '../movie_poster_list_widget.dart';
import '../loading_widget.dart';
import '../message_display_widget.dart';

class PopularMoviesCardWidget extends StatelessWidget {
  final MoviesCubit moviesCubit;
  const PopularMoviesCardWidget({super.key, required this.moviesCubit});

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      margin: const EdgeInsets.symmetric(horizontal: 4, vertical: 10),
      child: BlocBuilder(
        buildWhen: (prev, curr) =>
            curr is PopularMoviesInitialState ||
            curr is PopularMoviesLoadingState ||
            curr is PopularMoviesLoadedState ||
            curr is PopularMoviesErrorState,
        bloc: moviesCubit,
        builder: (context, state) {
          if (state is PopularMoviesEmptyState) {
            return const MessageDisplay(
              message: 'Getting movies data',
            );
          } else if (state is PopularMoviesLoadingState) {
            return const LoadingWidget();
          } else if (state is PopularMoviesLoadedState) {
            return MoviePosterListWidget(
              moviesData: moviesCubit.popularMoviesList,
              moviesCubit: moviesCubit,
              movieType: MovieType.popular,
            );
          } else if (state is PopularMoviesErrorState) {
            return MessageDisplay(
              message: state.message,
            );
          } else {
            return const SizedBox.shrink();
          }
        },
      ),
    );
  }
}
