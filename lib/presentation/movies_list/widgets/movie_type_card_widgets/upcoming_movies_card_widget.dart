import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:yologroup_assignment/presentation/movies_list/cubit/movies_cubit.dart';
import '../../../../common/utils/enums.dart';
import '../../cubit/movies_state.dart';
import '../movie_poster_list_widget.dart';
import '../loading_widget.dart';
import '../message_display_widget.dart';

class UpcomingMoviesCardWidget extends StatelessWidget {
  final MoviesCubit moviesCubit;
  const UpcomingMoviesCardWidget({super.key, required this.moviesCubit});

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      margin: const EdgeInsets.symmetric(horizontal: 4, vertical: 10),
      child: BlocBuilder(
        bloc: moviesCubit,
        buildWhen: (prev, curr) =>
            curr is UpcomingMoviesInitialState ||
            curr is UpcomingMoviesLoadingState ||
            curr is UpcomingMoviesLoadedState ||
            curr is UpcomingMoviesErrorState,
        builder: (context, state) {
          if (state is UpcomingMoviesEmptyState) {
            return const MessageDisplay(
              message: 'Getting movies data',
            );
          } else if (state is UpcomingMoviesLoadingState) {
            return const LoadingWidget();
          } else if (state is UpcomingMoviesLoadedState) {
            return MoviePosterListWidget(
                moviesData: moviesCubit.upcomingMoviesList,
                moviesCubit: moviesCubit,
                movieType: MovieType.upcoming);
          } else if (state is UpcomingMoviesErrorState) {
            return MessageDisplay(
              message: state.message,
            );
          } else {
            return MoviePosterListWidget(
                moviesCubit: moviesCubit, movieType: MovieType.upcoming);
          }
        },
      ),
    );
  }
}
