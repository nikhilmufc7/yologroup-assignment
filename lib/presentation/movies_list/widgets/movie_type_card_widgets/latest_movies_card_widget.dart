import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:yologroup_assignment/presentation/movies_list/cubit/movies_cubit.dart';

import '../../../../common/utils/enums.dart';
import '../../cubit/movies_state.dart';
import '../movie_poster_list_widget.dart';
import '../loading_widget.dart';
import '../message_display_widget.dart';

class LatestMoviesCardWidget extends StatelessWidget {
  final MoviesCubit moviesCubit;
  const LatestMoviesCardWidget({super.key, required this.moviesCubit});

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      margin: const EdgeInsets.symmetric(horizontal: 4, vertical: 10),
      child: BlocBuilder(
        buildWhen: (prev, curr) =>
            curr is LatestMoviesInitialState ||
            curr is LatestMoviesLoadingState ||
            curr is LatestMoviesLoadedState ||
            curr is LatestMoviesErrorState,
        bloc: moviesCubit,
        builder: (context, state) {
          if (state is LatestMoviesInitialState) {
            return const MessageDisplay(
              message: 'Getting movies data',
              key: ValueKey('message_display_widget_key_initial'),
            );
          } else if (state is LatestMoviesLoadingState) {
            return const LoadingWidget(
              key: ValueKey('loading_widget_key'),
            );
          } else if (state is LatestMoviesLoadedState) {
            return MoviePosterListWidget(
              moviesData: moviesCubit.latestMoviesList,
              moviesCubit: moviesCubit,
              movieType: MovieType.latest,
              key: const ValueKey('movies_card_key_widget'),
            );
          } else if (state is LatestMoviesErrorState) {
            return MessageDisplay(
              message: state.message,
              key: const ValueKey('message_display_widget_key_error'),
            );
          } else {
            return const SizedBox.shrink();
          }
        },
      ),
    );
  }
}
