import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:yologroup_assignment/presentation/movies_list/cubit/movies_cubit.dart';

import '../../../../common/utils/enums.dart';
import '../../cubit/movies_state.dart';
import '../movie_poster_list_widget.dart';
import '../loading_widget.dart';
import '../message_display_widget.dart';

class TopRatedMoviesCardWidget extends StatelessWidget {
  final MoviesCubit moviesCubit;
  const TopRatedMoviesCardWidget({super.key, required this.moviesCubit});

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      margin: const EdgeInsets.symmetric(horizontal: 4, vertical: 10),
      child: BlocBuilder(
        bloc: moviesCubit,
        buildWhen: (prev, curr) =>
            curr is TopRatedMoviesInitialState ||
            curr is TopRatedMoviesLoadingState ||
            curr is TopRatedMoviesLoadedState ||
            curr is TopRatedMoviesErrorState,
        builder: (context, state) {
          if (state is TopRatedMoviesEmptyState) {
            return const MessageDisplay(
              message: 'Getting movies data',
            );
          } else if (state is TopRatedMoviesLoadingState) {
            return const LoadingWidget();
          } else if (state is TopRatedMoviesLoadedState) {
            return MoviePosterListWidget(
              key: const ValueKey('top_rated_movies'),
              moviesData: moviesCubit.topRatedMoviesList,
              moviesCubit: moviesCubit,
              movieType: MovieType.topRated,
            );
          } else if (state is TopRatedMoviesErrorState) {
            return MessageDisplay(
              message: state.message,
            );
          } else {
            return MoviePosterListWidget(
              moviesCubit: moviesCubit,
              movieType: MovieType.topRated,
            );
          }
        },
      ),
    );
  }
}
