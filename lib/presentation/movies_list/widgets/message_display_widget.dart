import 'package:flutter/material.dart';

import '../../../common/widgets/snack_bar.dart';

class MessageDisplay extends StatelessWidget {
  final String? message;

  const MessageDisplay({
    Key? key,
    this.message,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Future.delayed(Duration.zero, () {
      CommonSnackBar.show(context, message ?? '');
    });
    return SizedBox(
      key: const ValueKey('message_display'),
      height: MediaQuery.of(context).size.height / 3,
      child: Center(
        child: SingleChildScrollView(
          child: Text(
            message ?? '',
            style: const TextStyle(fontSize: 25),
            textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }
}
