import 'package:flutter/material.dart';
import 'package:yologroup_assignment/common/constants/constants.dart';
import 'package:yologroup_assignment/presentation/movie_details/pages/movie_details_page.dart';
import '../../../data/movies_list/models/movies_data_model.dart';

class MoviePosterCardWidget extends StatelessWidget {
  final MovieResults? items;
  const MoviePosterCardWidget({Key? key, this.items}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      return InkWell(
        onTap: () {
          showModalBottomSheet(
              context: context,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              builder: (builder) {
                return Container(
                    height: MediaQuery.of(context).size.height / 1.6,
                    color: Colors.transparent,
                    child: MovieDetailsPage(
                      movieId: items!.id!,
                    ));
              });
        },
        child: Container(
          width: 120,
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(5),
                child: Image.network(
                  "${Constants.movieBannerPath}${items?.poster_path}",
                  // height: MediaQuery.of(context).size.height / 5.2,
                  width: 120,
                  fit: BoxFit.cover,
                ),
              ),
              const SizedBox(
                height: 5,
              ),
              Text(
                items?.title ?? '',
                textAlign: TextAlign.left,
                maxLines: 2,
              ),
            ],
          ),
        ),
      );
    });
  }
}
