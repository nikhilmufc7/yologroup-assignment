import 'package:flutter/material.dart';
import '../../../data/movie_details/models/movie_details_model.dart';

class MovieInfoRowWidget extends StatelessWidget {
  final MovieDetailsModel movieDetails;
  const MovieInfoRowWidget({super.key, required this.movieDetails});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                const Text("IMDB Rating:  "),
                Text(
                  movieDetails.vote_average.toString(),
                  style: const TextStyle(
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ],
            ),
            Row(
              children: [
                const Text("Release Date:  "),
                Text(
                  movieDetails.release_date!,
                  style: const TextStyle(
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ],
            ),
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                const Text('Revenue: '),
                Text(
                  "\$${movieDetails.revenue.toString()}",
                  style: const TextStyle(
                    fontWeight: FontWeight.w500,
                  ),
                )
              ],
            ),
            Row(
              children: [
                const Text('Adult: '),
                Text(
                  movieDetails.adult! ? "Yes" : "No",
                  style: const TextStyle(
                    fontWeight: FontWeight.w500,
                  ),
                )
              ],
            ),
          ],
        )
      ],
    );
  }
}
