import 'package:flutter/material.dart';
import 'package:yologroup_assignment/data/movie_details/models/movie_details_model.dart';

import '../../../common/constants/constants.dart';

class MovieDetailsImageWidget extends StatelessWidget {
  final MovieDetailsModel movieDetails;
  const MovieDetailsImageWidget({super.key, required this.movieDetails});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height / 4.5,
      child: Stack(
        children: [
          ClipRRect(
            borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(10), topRight: Radius.circular(10)),
            child: Image.network(
                "${Constants.movieBannerPath}${movieDetails.poster_path}",
                height: MediaQuery.of(context).size.height / 4.5,
                width: MediaQuery.of(context).size.width,
                fit: BoxFit.cover),
          ),
          Visibility(
            visible: movieDetails.video!,
            child: Align(
              alignment: Alignment.center,
              child: InkWell(
                  onTap: () {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text(movieDetails.original_title ?? '')));
                  },
                  child: const Icon(
                    Icons.play_arrow,
                    size: 32,
                    color: Colors.white,
                  )),
            ),
          )
        ],
      ),
    );
  }
}
