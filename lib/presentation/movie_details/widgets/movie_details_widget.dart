import 'package:flutter/material.dart';
import 'package:yologroup_assignment/data/movie_details/models/movie_details_model.dart';

import 'movie_details_image_widget.dart';
import 'movie_info_row_widget.dart';

class MovieDetailsWidget extends StatelessWidget {
  final MovieDetailsModel movieDetails;

  const MovieDetailsWidget({
    Key? key,
    required this.movieDetails,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MovieDetailsImageWidget(
          movieDetails: movieDetails,
        ),
        const SizedBox(
          height: 10,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                movieDetails.title ?? '',
                style:
                    const TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                movieDetails.overview ?? '',
                maxLines: 6,
                style:
                    const TextStyle(fontWeight: FontWeight.w400, fontSize: 12),
              ),
              const SizedBox(
                height: 10,
              ),
              MovieInfoRowWidget(
                movieDetails: movieDetails,
              ),
            ],
          ),
        )
      ],
    );
  }
}
