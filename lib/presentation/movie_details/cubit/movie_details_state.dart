import 'package:equatable/equatable.dart';
import 'package:yologroup_assignment/data/movie_details/models/movie_details_model.dart';
import 'package:meta/meta.dart';

@immutable
abstract class MovieDetailsState extends Equatable {
  @override
  List<Object> get props => [];
}

class MovieDetailsEmptyState extends MovieDetailsState {}

class MovieDetailsInitialState extends MovieDetailsState {}

class MovieDetailsLoadedState extends MovieDetailsState {
  final MovieDetailsModel movieDetails;

  MovieDetailsLoadedState({required this.movieDetails});

  @override
  List<Object> get props => [movieDetails];
}

class MovieDetailsLoadingState extends MovieDetailsState {}

class MovieDetailsErrorState extends MovieDetailsState {
  final String message;

  MovieDetailsErrorState({required this.message});

  @override
  List<Object> get props => [message];
}
