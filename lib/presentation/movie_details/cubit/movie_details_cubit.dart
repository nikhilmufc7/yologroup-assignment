import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:yologroup_assignment/domain/movie_details/usecase/movie_details_usecase.dart';
import 'movie_details_state.dart';

class MovieDetailsCubit extends Cubit<MovieDetailsState> {
  final GetMovieDetailsUsecase getMovieDetailsUsecase;

  MovieDetailsCubit(this.getMovieDetailsUsecase)
      : super(MovieDetailsInitialState());

  Future<void> getMovieDetails(int movieId) async {
    emit(MovieDetailsLoadingState());

    try {
      final result = await getMovieDetailsUsecase.call(Params(movieId));

      result.fold(
          (failure) =>
              emit(MovieDetailsErrorState(message: failure.toString())),
          (repositoriesData) =>
              emit(MovieDetailsLoadedState(movieDetails: repositoriesData)));
    } catch (e) {
      emit(MovieDetailsErrorState(message: e.toString()));
    }
  }
}
