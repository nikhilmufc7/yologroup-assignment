import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:yologroup_assignment/dependency_injection.dart';
import 'package:yologroup_assignment/presentation/movies_list/widgets/loading_widget.dart';
import 'package:yologroup_assignment/presentation/movies_list/widgets/message_display_widget.dart';
import 'package:yologroup_assignment/presentation/movie_details/cubit/movie_details_cubit.dart';
import 'package:yologroup_assignment/presentation/movie_details/widgets/movie_details_widget.dart';

import '../cubit/movie_details_state.dart';

class MovieDetailsPage extends StatefulWidget {
  final int movieId;
  const MovieDetailsPage({
    Key? key,
    required this.movieId,
  }) : super(key: key);

  @override
  State<MovieDetailsPage> createState() => _MovieDetailsPageState();
}

class _MovieDetailsPageState extends State<MovieDetailsPage> {
  late MovieDetailsCubit _movieDetailsCubit;

  @override
  void initState() {
    super.initState();
    _movieDetailsCubit = sl.get();
    _movieDetailsCubit.getMovieDetails(widget.movieId);
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => sl<MovieDetailsCubit>(),
      child: Column(
        children: <Widget>[
          BlocBuilder(
            bloc: _movieDetailsCubit,
            builder: (context, state) {
              if (state is MovieDetailsInitialState) {
                return const MessageDisplay(
                  message: 'This is empty...',
                );
              } else if (state is MovieDetailsLoadingState) {
                return const LoadingWidget();
              } else if (state is MovieDetailsLoadedState) {
                return MovieDetailsWidget(
                  movieDetails: state.movieDetails,
                );
              } else if (state is MovieDetailsErrorState) {
                return MessageDisplay(
                  message: state.message,
                );
              } else {
                return Container();
              }
            },
          ),
          // const SizedBox(height: 20),
          // Bottom half
        ],
      ),
    );
  }
}
