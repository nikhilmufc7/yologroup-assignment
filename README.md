# yologroup_assignment

 Overview

- Follows clean code architecture

- Includes Unit tests and Widget tests for Latest Movies

- Populates first two section and load and other sections are collapsed

- Polls latest movies every 30s when not collapsed

- Built following test driven development wherever possible

- Null safety in complete project also in test-cases!

- Tried to cover as much of test cases as I could in the time frame.

- Uses flutter bloc for state management

- Follows reactive & functional programming in whole project using Dartz

- Integration of Five APIs

- All features are segregated into separate components that can be easily reused wherever possible

- Have kept it mostly lean in terms of usage of external packages


<p float="left">
  <img src="./screenshots/1.png" width="100" />
  <img src="./screenshots/2.png" width="100" />
  <img src="./screenshots/3.png" width="100" />
  <img src="./screenshots/4.png" width="100" />
</p>